# MLIP Reporting Checklist

The checklist is found here as a PDF of the most recent version and an HTML file which can be modified.  We encourage users to make pull requests to suggest additions, clarifications, or removals to the checklist via the HTML file.  Code-specific additions may be added in the future by a drop-down selection which hides or shows specific items.

TODO: Setup automatic rendering of PDF (and code dependent versions) from HTML via Webhook.
